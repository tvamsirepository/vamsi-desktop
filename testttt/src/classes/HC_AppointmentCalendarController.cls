public class HC_AppointmentCalendarController {
    public string eventsform{set;get;}
    public datetime startdate{set;get;}
    public boolean displayevent{get;set;}
    public time selectedtime {set;get;}
    public String selecteddt {set;get;}
    public string selectdt{set;get;}
    public string stage{set;get;}
  //  Public string Timezone{set;get;}
    public boolean manualtimeslot {set;get;}
    public Boolean displaypopup{set;get;} 
    public Boolean displaypopup2{set;get;}
    public Boolean displayPopUp3{set;get;}
    public Boolean displayPopUp4{set;get;}
    public Boolean displayPopUp5{set;get;}
    public blob Attach{set;get;} 
    public Boolean includeMyEvents {get;set;}
    public list<calEvent> events {get;set;}
    
    public integer totalevents{set;get;}
    
    String dtFormat = 'd MMM yyyy HH:mm:ss z';
    
    public List<contact> UserTemp = new List<contact>();
    public String selectedUser { get; set; }
    
    public HC_AppointmentCalendarController()
    {
        includeMyEvents = true;
        
        contact d=[select id,name from contact where id=:apexpages.currentPage().getParameters().get('id')];
        list<Event> TotalRec = [select ID,Subject,WhoId,WhatId,OwnerId,StartDateTime,EndDateTime from Event where WhoId = :d.Id];
        totalevents = TotalRec.size();
        
    } 
    //showing the popup window for create a event
    public void showPopup()
    {       
        displayPopup = true; 
        displaypopup2=true;
    }  
    public PageReference toggleMyEvents() {
        if(includeMyEvents){
            includeMyEvents = false;
        }
        else{
            includeMyEvents = true;
        }
        pageload();
        return null;
    }
   public contact d=[select id,name from contact where id=:apexpages.currentPage().getParameters().get('id')]; 
    public void submit()
    {
        
        string selectmanualtime = string.valueOf(selectedtime);
        string starttime = selectdt+' '+selectmanualtime;
        displaypopup=false;
        if(selectdt>=String.valueof(system.today()))
         {
             
            user u = [select id, name, contactid from user where id =: userinfo.getUserId()];   
            list<Event> TotalRec = [select ID,Subject,WhoId,WhatId,OwnerId,StartDateTime,EndDateTime from Event where Start_Date__c =:selectdt and WhoId =:d.Id];
            system.debug('selectdt'+selectdt);
            system.debug('Today'+ system.today());
          
            if(TotalRec.size() <= 4)
            {   
               // if(stage == 'Pending')
               // {
                     Event Event =new Event();
                    
                        
                  
                   // contact c = [select id, name, phone, email from contact where id =: u.ContactId];
                   
                    Event.Subject='Appointment';
                    Event.Location='Hyderabad';
                    Event.OwnerId='00528000005dSML';
                   // Event.WhoId = u.ContactId;
                    Event.Start_Date__c=selectdt;
                      Event.Event_Status__c='Pending';
                  //  Event.Event_Status__c=Stage;
                    Event.StartDateTime= Datetime.valueOf(starttime);
                    Event.EndDateTime=Event.StartDateTime.addhours(4);
                   // Event.Timezone__c=Timezone;
                    Event.IsVisibleInSelfService = TRUE;
                    Event.Community_User_Owner__c = Userinfo.getUserId();
                    system.debug('Event'+Event);
                    event.WhoId = d.id;
                    insert Event;
                    //ApexPages.addMessage(new ApexPages.message(ApexPages.severity.CONFIRM,'Event created succesfully'));
                    displayPopUp5=true;
               // }
                
            //    else{
                    //ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Info,'Please select stage as  pending '));
                //    displaypopup3=true;
              //  }
            }
            else
            {
                //ApexPages.addMessage(new ApexPages.message(ApexPages.severity.WARNING,'Event already exists'));
                displayPopUp4=true;
            }
        }
        
       else{
            displaypopup=false;
            displaypopup2=true;                
        }
        
        }
    
    public PageReference pageLoad() {
        
        events = new list<calEvent>();
        if(includeMyEvents)
        {
            
            for(Event Mypaln1 :[select Id,StartDateTime,EndDateTime, Subject, Location, WhoId, WhatId  from Event where Event_Status__c='Confirmed' and WhoId=:d.id ]){//and WhoId=:d.id
                
                datetime startDT = Mypaln1.StartDateTime;            
                datetime endDT = Mypaln1.EndDateTime;
                calEvent MypalnEvent1 =new calEvent();  
                MypalnEvent1.title = 'Event:'+ Mypaln1.subject+ ' ';
                MypalnEvent1.allDay = true;
                MypalnEvent1.startString = startDT;
                MypalnEvent1.endString = endDT;
                MypalnEvent1.url = '/' + Mypaln1.Id;
                MypalnEvent1.className = 'event-Confirmed';
                //MypalnEvent1.owner = '00528000005dSML';
                events.add(MypalnEvent1);
                
            }
            
            for(Event Mypaln2 :[select Id,StartDateTime,EndDateTime, Subject, Location, WhoId, WhatId  from Event where Event_Status__c='pending' and WhoId=:d.id ]){//and WhoId=:d.id
                datetime startDT = Mypaln2.StartDateTime;            
                datetime endDT = Mypaln2.EndDateTime;
                calEvent MypalnEvent2 =new calEvent();  
                MypalnEvent2.title = 'Event:'+ Mypaln2.subject+ ' ';
                MypalnEvent2.allDay = true;
                MypalnEvent2.startString = startDT;
                MypalnEvent2.endString = endDT;
                MypalnEvent2.url = '/' + Mypaln2.Id;
                MypalnEvent2.className = 'event-Pending';
                events.add(MypalnEvent2);
                
            }
            
        }
        return null;
    }
    
    public void Deleteevent(){
        string selectmanualtime = string.valueOf(selectedtime);
        string starttime = selectdt+' '+selectmanualtime;
        Event e=[select id,StartDateTime,EndDateTime,Start_Date__c from Event where Start_Date__c=:selectdt];
        delete e;
       ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Confirm,'Event Deleted succesfully'));
    }
    public void UpdateEvent()
    {
        Event e=[select id,StartDateTime,EndDateTime,Start_Date__c,Event_Status__c from Event where Start_Date__c=:selectdt];
        if(e.Event_Status__c== 'Pending')
        {
            if(stage == 'Confirmed'){
                e.Event_Status__c='Confirmed';
                update e;
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Confirm,'Event Updated succesfully'));
            }
            else{
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Info,'Please select stage as confirmed to update'));
            }
        }
        else{
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Info,'The stage cant be updated for already completed events'));  
        }
    }
    
    
    
    //Class to hold calendar event data
    public void GetEvents(){
        
    }
    public void cancelevent(){
        displaypopup=false;
        displaypopup2=false; 
        displaypopup3=false;
        displaypopup4=false; 
        displayPopUp5=false;
    }
    public void Rerendertop(){
        displaypopup=true;
        
    }
    public class calEvent{
        public String title {get;set;}
        public Boolean allDay {get;set;}
        public string statString {get;set;}
        public string edString {get;set;}
        public dateTime startString {get;set;}
        public dateTime endString {get;set;}
        public String url {get;set;}
        public String className {get;set;}     
    }
    
}