public class DoctorInformationController {

    private final Contact ct;
    
    
    public DoctorInformationController(ApexPages.StandardController stdController) {
        this.ct = (Contact)stdController.getRecord();
    }
    
    public pagereference bookap(){
        pagereference pr=new pagereference('/apex/HC_AppointmentCalendar/?');
        return pr;
         }
}