global with sharing class HC_CustomLoginController {
    global String username{get;set;}
    global String password{get;set;}
    global HC_CustomLoginController () {}
    global PageReference forwardToCustomAuthPage() {
        return new PageReference( '/CustomLogin');
    }
    global PageReference login() {
        return Site.login(username, password, null);
    }
   
}