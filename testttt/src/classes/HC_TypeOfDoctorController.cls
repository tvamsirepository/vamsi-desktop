global class HC_TypeOfDoctorController {
    public string type{set;get;}
    public string City{set;get;}
    public string zip{set;get;}
    public id docId{set;get;}
  
    
String query = '';
    String conditions ='';
    //public list<Doctor__c> searchedRecords{set;get;}
    list<contact> docs = new list<contact> ();
    global void search(){

         query = 'select id,name,Type_of_Doctor__c,City__c from contact';
        conditions = ' WHERE id != null';
       
       if(type != null) {
           type = String.escapeSingleQuotes(type);
           conditions = conditions+' and '+'Type_of_Doctor__c Like \'%'+type+'%\' ';
       }
       if(City != '') {
           City = String.escapeSingleQuotes(City);
           conditions = conditions+' and '+'City__c Like \'%'+City+'%\' ';
       }
        
      //  if(zip != '') {
        //   zip = String.escapeSingleQuotes(zip);
          // conditions = conditions+' and '+'Zip_Postal_Code__c Like \'%'+zip+'%\' ';
       //} 
System.debug('conditions :: '+conditions);
       query = query+conditions+' LIMIT 20';
       System.debug('query :: '+query);
       
       docs = Database.query(query);
       System.debug('docs :: '+docs);
     
type ='';
        City='';
       // zip='';
        getsearchedRecords();
    }
    global list<contact> getsearchedRecords(){
        system.debug('getsearchedRecords' );
        //refresh = true;
        return docs;
    }
    global pagereference redirect(){
        pagereference pr = new pagereference('/apex/DoctorInfo?id='+docId);
        return pr;
        
    }

}