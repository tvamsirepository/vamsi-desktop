public class DisplayPatientsListController {
    
    public list<Patients__c> patientrecords{set;get
    {
         user u=[select name,id,ContactId from user where id =: userinfo.getUserId()];
        
        string doctorid=u.ContactId;
        
    return [select id,name from Patients__c where contactdoctor__c =:doctorid] ;  
         
    }}
    public DisplayPatientsListController()
    {
       user u=[select name,ContactId from user where id =: userinfo.getUserId()];
        
        string doctorid=u.ContactId;
        
    patientrecords=[select id,name from Patients__c where contactdoctor__c =:doctorid] ;  
       
    }
    
}