trigger HC_PopulateViewPatient on Event (after update) {
    //Patients__c p=new Patients__c();
    for(event e:trigger.new){
        
        if(e.Event_Status__c == 'Confirmed'){
        //p.View_Patient__c = e.Community_User_Owner__c;
        doctor__c d= [select id, View_Doctor__c from doctor__c where View_Doctor__c =:e.WhoId];
            user u=[select id,contactid from user where id=:e.Community_User_Owner__c limit 1];
       //Patients__c p=[select id,View_Patient__c,doctor__c from Patients__c where View_Patient__c = :u.contactid limit 1];
       	Patients__c p=[select id,View_Patient__c,doctor__c from Patients__c where View_Patient__c = :u.contactid ];
            p.doctor__c = d.Id;
            p.contactdoctor__c =e.WhoId;
            update p;
        }
        
    }
    
}